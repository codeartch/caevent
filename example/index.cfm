﻿<cfsilent>
	<cffunction name="FormateCode" access="private" returntype="string">
		<cfargument name="code" required="true" type="string">
		
		<cfset arguments.code = ReplaceNoCase(arguments.code,"<","&lt;","all")>
		<cfset arguments.code = ReplaceNoCase(arguments.code,">","&gt;","all")>
		<cfset arguments.code = "<i>#arguments.code#</i>">
		
		<cfreturn arguments.code>
	</cffunction>
</cfsilent>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>CAEvent</title>
</head>

<body>
	
<h1>CAEvent</h1>

CAEvent provides a simple event driven framework to allow a more event-based, asynchronous approach to developing ColdFusion applications.<br />
The structure of the framework is based on AS3 and Javascript event handling.

<h2>Installation</h2>
<cfset code1 = '<cfset application.caevent = StructNew()>'>
<cfset code2 = '<cfset application.caevent.eventManager	= CreateObject("component","cfc.caevent.EventManager").init()>'>
<cfoutput>
	#FormateCode(code1)#<br />
	#FormateCode(code2)#
</cfoutput>

<h2>Creating a Event</h2>
<cfset code1 = '<cfset e = application.caevent.EventManager.new("example2_event")>'>
<cfoutput>
	#FormateCode(code1)#<br />
</cfoutput>

<h2>Add listeners</h2>
<cfset code1 = '<cfset e.addListener(application.exampleObj,"run1")>'>
<cfoutput>
	#FormateCode(code1)#<br />
</cfoutput>
<br />Adds a listener to the event (object="application.exampleObj", function="run1")

<h2>Dispatch event</h2>
<cfset code1 = '<cfset returndata = e.dispatch({name="edie",lastname="britt"})>'>
<cfoutput>
	#FormateCode(code1)#<br />
</cfoutput>
<br />Dispatch the event with two parameters (name="edie",lastname="britt")
<br />The parameters are passed to the listeners.

<br /><br /><strong>More examples are included in the download.</strong>

<ul>
	<li><a href="http://caevent.riaforge.org/index.cfm?event=action.download" target="_blank">Download</a></li>
	<li><a href="http://caevent.riaforge.org/index.cfm?event=page.issues" target="_blank">Bugtracker</a></li>
	<li><a href="/cfcdoc" target="_blank">CAEvent CFC Doc</a></li>
	<li><a href="/source/example1.cfm?init" target="_blank">Online Example 1</a></li>
	<li><a href="/source/example2.cfm?init" target="_blank">Online Example 2</a></li>
</ul>

<h2>Good Links</h2>
<ul>
	<li><a href="http://www.dustinmartin.net/2010/04/thoughts-on-event-driven-programming/" target="_blank">Thoughts on Event Driven Programming</a></li>
	<li><a href="http://tv.adobe.com/watch/max-2008-develop/driven-programming-in-coldfusion-by-sean-corfield/" target="_blank">Driven Programming In ColdFusion by Sean Corfield on Adobe TV</a></li>
	<li><a href="http://edmund.riaforge.org/" target="_blank">Edmund Framework</a></li>
</ul>

</body>
</html>