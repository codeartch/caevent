<cfcomponent output="false" hint="Demo Listeners">
	
<!--- 
	SYNC demo functions
--->	
	
	<cffunction name="run1" access="public" returntype="struct" output="false" hint="">
		<cfargument name="name" required="true" type="string" hint="FIX ARGUMENTS">
		<cfargument name="lastname" required="true" type="string" hint="FIX ARGUMENTS">
		<cfargument name="eventData" required="false" type="struct" hint="OPTIONAL">
		<cfargument name="rawEventData" required="false" type="struct" hint="OPTIONAL">
		
		<!--- Change event vars --->
		<cfset arguments.name = "Britney">
		<cfset arguments.lastname = "Spears">
		
		<!--- Sync functions return --->
		<cfreturn {name=arguments.name,lastname=arguments.lastname} />
	</cffunction>
	
	
	<cffunction name="run2" access="public" returntype="struct" output="false" hint="">
		<cfargument name="name" required="true" type="string" hint="FIX ARGUMENTS">
		<cfargument name="lastname" required="true" type="string" hint="FIX ARGUMENTS">
		<cfargument name="eventData" required="false" type="struct" hint="OPTIONAL">
		<cfargument name="rawEventData" required="false" type="struct" hint="OPTIONAL">
		
		<!--- No event var changes --->
		
		<!--- Sync functions return --->
		<cfreturn {name=arguments.name,lastname=arguments.lastname} />
	</cffunction>
	
	
	<cffunction name="run3" access="public" returntype="struct" output="false" hint="">
		<cfargument name="name" required="true" type="string" hint="FIX ARGUMENTS">
		<cfargument name="lastname" required="true" type="string" hint="FIX ARGUMENTS">
		<cfargument name="eventData" required="false" type="struct" hint="OPTIONAL">
		<cfargument name="rawEventData" required="false" type="struct" hint="OPTIONAL">
		
		<!--- No event var changes --->
		
		<!--- Sync functions return --->
		<cfreturn {name=arguments.name,lastname=arguments.lastname} />
	</cffunction>


<!--- 
	ASYNC demo functions
--->		
	
	<cffunction name="run4" access="public" returntype="void" output="false" hint="Async Call: Return Void da Async Call">
		<cfargument name="name" required="true" type="string" hint="FIX ARGUMENTS">
		<cfargument name="lastname" required="true" type="string" hint="FIX ARGUMENTS">
		<cfargument name="eventData" required="false" type="struct" hint="OPTIONAL">
		<cfargument name="rawEventData" required="false" type="struct" hint="OPTIONAL">
		
		<!--- Set application flag --->
		<cfset application.status = "async run4 done">
		
		<!--- Async functions have no return ! --->
	</cffunction>
	
</cfcomponent>