<h1>CAEvent, example2</h1>


<!--- EventManager --->
<cfset em = application.caevent.manager>

<cfif em.exists("example2_event")>
	Event "example2_event" already exists.<br />
	Add "?init" to the URL to force a reinitialisation.
<cfelse>		
	<h2>Event with listeners</h2>
	<!--- Create a new Event --->
	<cfset e = em.new('example2_event')>
	
	<!--- Add listeners directly to a event --->
	<cfset e.addListener(application.exampleObj,"run1")><!--- default sync listener --->
	<cfset e.addListener(application.exampleObj,"run2","key2")><!--- sync listener with a key (key is needed for unregister) --->
	<cfset e.addListener(application.exampleObj,"run4","",true)><!--- async listener (key is needed for unregister) --->
	
	<!--- Remove listeners directly from a event --->
	<cfset e.removeListener("key2")><!--- remove by key --->
	<cfdump var="#e#">
	
	<!--- Dispatch event directly with two Vars (name,lastname) --->
	<h2>Return values (only sync events)</h2>
	<cfset returndata = e.dispatch({name="edie",lastname="britt"})>
	<!--- Save event return data into returndata variable (return values are only from sync events)--->
	<cfdump var="#returndata#">
	
	<!--- Async event threads/status --->
	<h2>Event Threads (only async events)</h2>
	<cfdump var="#e.getThread()#"><br />
	Status: <cfdump var="#application.status#">
</cfif>


