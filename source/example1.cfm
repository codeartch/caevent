<h1>CAEvent, example1</h1>

<!--- EventManager --->
<cfset em = application.caevent.manager>

<cfif em.exists("example1_event")>
	Event "example1_event" already exists.<br />
	Add "?init" to the URL to force a reinitialisation.
<cfelse>		
	<h2>Event with listeners</h2>
	<!--- A new event is created everytime a the firs listern register for it --->
	
	<!--- Register listeners trough event manager --->
	<cfset em.register("example1_event",application.exampleObj,"run1")><!--- default sync listener --->
	<cfset em.register("example1_event",application.exampleObj,"run2","key2")><!--- sync listener with a key (key is needed for unregister) --->
	<cfset em.register("example1_event",application.exampleObj,"run3")><!--- default sync listener --->
	<cfset em.register("example1_event",application.exampleObj,"run4","",true)><!--- async listener (key is needed for unregister) --->
	
	<!--- Unregister listeners trough event manager --->
	<cfset em.unregister("example1_event","key2")>
	
	<cfdump var="#em.get('example1_event')#">
	
	<!--- Dispatch event trough event manager, with two Vars (name,lastname) --->
	<h2>Return values (only sync events)</h2>
	<cfset returndata = em.dispatch("example1_event",{name="john",lastname="sutter"})>
	<!--- Save event return data into returndata variable (return values are only from sync events)--->
	<cfdump var="#returndata#">
	
	<h2>Status (only async events)</h2>
	- <cfdump var="#application.status#">
</cfif>


