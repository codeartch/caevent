<cfcomponent displayname="Application Component" output="false">
	
	<cfset this.name 					= "caevent_application_01">
	<cfset this.applicationtimeout 		= CreateTimeSpan(1,0,0,0)>


    <cffunction name="onApplicationStart" output="no" hint="This fires when the application first runs. It is a single-threaded method call">
		
		<!--- Only for example purposes --->
		<cfscript>
			application.status 					= "No Status";
			application.exampleObj				= CreateObject("component","ExampleListeners");
		</cfscript>	
		
		<!--- Needed to initialize the CAEvent Framework --->
		<cfscript>
			application.caevent.manager			= CreateObject("component","cfc.caevent.Manager").init();
		</cfscript>
		
    	<cfreturn />
    </cffunction>
	
	
	<cffunction name="onSessionStart" returntype="void" output="no" hint="This fires when an individual session first runs. It is a single-threaded method call">
		
    </cffunction>
    
	
 	<cffunction name="OnRequestStart" returntype="boolean" output="true" hint="This fires when a page request first runs. It is a single-threaded method call">
		
		<cfif isDefined("url.init")>
			<cfset onApplicationStart()>
		</cfif>
		
    	<cfreturn true />
    </cffunction>
	
	
	<cffunction name="OnRequest" output="true" returntype="void" hint="This fires when the requested template needs to get processed">
		<cfargument name="TargetPage" type="string" required="yes" />
		
		<cfinclude template="#arguments.TargetPage#">	
		
	</cffunction>
	
	
    <cffunction name="OnRequestEnd" returntype="void" output="no" hint="This fires at the end of every page request. It is a single-threaded method call">

		<cfreturn />
    </cffunction>
    
	
	<cffunction name="onSessionEnd" returnType="void" output="no" hint="This fires at the end of every session. This will fire either for a session timeout or if the server is shutting down. It is a single-threaded method call">
		<cfargument name="SessionScope" type="struct" required="yes" />
 		<cfargument name="ApplicationScope" type="struct" required="no" default="#StructNew()#" />
 
		<cfreturn />
	</cffunction>


	<cffunction name="onApplicationEnd" output="no" hint="This fires at the end of the application. This will fire for an application timeout of if the server is shutting down. It is a single-threaded method call">
		<cfargument name="ApplicationScope" type="struct" required="no" default="#StructNew()#" />
 
		<cfreturn />
	</cffunction>
		
		
	<cffunction name="OnError" returntype="void" hint="This fires if an exception gets thrown and is not caught by the controlling code" output="yes">
		<cfargument name="Exception" type="any" required="yes" />
		<cfargument name="EventName" type="string" required="no" default="" />	

		<cfdump var="#arguments.Exception#"><cfabort>

		<cfreturn />
    </cffunction>
	
</cfcomponent>