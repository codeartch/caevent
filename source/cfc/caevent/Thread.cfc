<cfcomponent output="no" name="Thread" hint="Manages async threads">
	
	<cffunction name="init" access="public" returntype="struct" output="no" hint="Initializes the object">
		
		<cfset variables.thread 	= ArrayNew(1)>
		<cfset variables.javaThread = CreateObject("java","java.lang.Thread")>
		
		<cfreturn this />
    </cffunction>
	
	
	<cffunction name="asyncInvoke" access="public" returntype="void" output="no" hint="Dispatch an event asynchron">
		<cfargument name="object" type="component" required="true" hint="Object in which a function should be called">
		<cfargument name="method" type="string" required="true" hint="Function name which should be called">
		<cfargument name="event" type="Event" required="true" hint="Event object">
		<cfargument name="eventData" type="any" required="true" hint="Event data">
		<cfargument name="rawEventData" type="any" required="true" hint="Raw event data">
		
		<cfset local.threadName = variables.javaThread.currentThread().getThreadGroup().getName() />
		
		<cfif threadName eq "cfthread" or threadName eq "scheduler">

			<cfinvoke component="#arguments.object#" method="#arguments.method#">
				<cfinvokeargument name="argumentCollection" value="#arguments.eventData#">
				<cfinvokeargument name="event" value="#arguments.event#">
				<cfinvokeargument name="rawEventData" value="#arguments.rawEventData#">
			</cfinvoke>

		<cfelse>
			<cfset local.name = "caevent_thread_#arguments.event.name#_#Now()#">
			<cfset ArrayAppend(variables.thread,local.name)>
			
			<cfthread action="run" name="#local.name#" arguments="#arguments#" threadname="#local.name#">
				<cfinvoke component="#attributes.arguments.object#" method="#attributes.arguments.method#">
					<cfinvokeargument name="argumentCollection" value="#attributes.arguments.eventData#">
					<cfinvokeargument name="event" value="#attributes.arguments.event#">
					<cfinvokeargument name="rawEventData" value="#attributes.arguments.rawEventData#">
				</cfinvoke>
				
				<cfset ArrayDelete(variables.thread,attributes.threadname)>
			</cfthread>
		</cfif>

	</cffunction>
	
		
	<cffunction name="getThread" access="public" returntype="array" output="no" hint="Returns all active threads">
			
		<cfreturn variables.thread />
	</cffunction>
	
</cfcomponent>