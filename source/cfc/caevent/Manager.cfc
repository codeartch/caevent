<!---
	Autor:		Codeart, Simon Lampert
	Website:	www.codeart.ch
	Docs		doc.codeart.ch/?project=caevent
	Release:	5.04.2011
	Version:	1.0 (Release)
	License:	Common Public License (http://www.opensource.org/licenses/cpl1.0.php)
	
	
	Testet on:	Railo 3.2, Coldfusion 9.0.1
--->

<cfcomponent output="no" name="Manager" hint="Manages event instances">
	
	<cffunction name="init" access="public" returntype="struct" output="no" hint="Initializes the object">
		
		<cfset variables.instance 								= StructNew()>
		<cfset variables.event 									= StructNew()>
		<!--- CAEvent Setting--->
		<cfset variables.instance.setting 						= StructNew()>
		<!--- Default event object to use --->
		<cfset variables.instance.setting.defaultEventObject 	= "Event">
		<!--- Allow async events --->
		<cfset variables.instance.setting.allowAsync 			= true>
		<!--- Debug Mode shows detailed variable infos for each called listener in the returndata  --->
		<cfset variables.instance.setting.debugMode 			= true>
		
		<cfreturn this />
    </cffunction>
	

	<cffunction name="register" access="public" returntype="boolean" output="no" hint="Adds a listener to the event with the given namet">
		<cfargument name="eventName" type="string" required="true" hint="Event name">
		<cfargument name="object" type="component" required="true" hint="Object in which a function should be called">
		<cfargument name="method" type="string" required="true" hint="Function name which should be called">
		<cfargument name="key" type="string" required="false" default="" hint="Key, used to remove a listener">
		<cfargument name="async" type="boolean" required="false" default="false" hint="Execute event asynchron (no callback)">
		
		<cfset local.returnData = true>
		
		<cfif not exists(arguments.eventName)>
			<cfset new(arguments.eventName)>
		</cfif>
		<cfset variables.event[arguments.eventName].object.addListener(arguments.object,arguments.method,arguments.key,arguments.async)>
		
		<cfreturn local.returnData />
	</cffunction>
	
	
	<cffunction name="unregister" access="public" returntype="boolean" output="no" hint="Removes a listener from the event with the given name">
		<cfargument name="eventName" type="string" required="true" hint="Event name">
		<cfargument name="key" type="string" required="true" hint="Listener key">
		
		<cfset local.returnData = true>
		
		<cfif exists(arguments.eventName)>
			<cfset variables.event[arguments.eventName].object.removeListener(arguments.key)>
		<cfelse>
			<cfset local.returnData = false>
		</cfif>
		
		<cfreturn local.returnData />
	</cffunction>


	<cffunction name="dispatch" access="public" returntype="any" output="no" hint="Dispatches the event with the given name">
		<cfargument name="eventName" type="string" required="true" hint="Event name">
		<cfargument name="eventData" type="any" required="false" default="" hint="Event data">
		
		<cfif exists(arguments.eventName)>
			<cfset local.returnData = variables.event[arguments.eventName].object.dispatch(arguments.eventData)>
		<cfelse>
			<cfset local.returnData = "No Event found">
		</cfif>
		
		<cfreturn local.returnData />
	</cffunction>
	
	
	<cffunction name="new" access="public" returntype="Event" output="no" hint="Creates a new event with the given name">
		<cfargument name="eventName" type="string" required="true" hint="Event name">
		
		<cfif not exists(arguments.eventName)>
			<cfset variables.event[arguments.eventName]			= StructNew()>
			<cfset variables.event[arguments.eventName].object 	= CreateObject("component","Event").init(arguments.eventName,this)>
		<cfelse>
			<cfdump var="Event name '#arguments.eventName#' already exists"><cfabort>
		</cfif>
		
		<cfreturn variables.event[arguments.eventName].object />
	</cffunction>
	
	
	<cffunction name="get" access="public" returntype="Event" output="no" hint="Returns the event with the given name">
		<cfargument name="eventName" type="string" required="true" hint="Event name">
		
		<cfif exists(arguments.eventName)>
			<cfset local.returnData	= variables.event[arguments.eventName].object>
		<cfelse>
			<cfdump var="Event '#arguments.eventName#' does not exists"><cfabort>
		</cfif>
		
		<cfreturn local.returnData />
	</cffunction>
	

	<cffunction name="getAll" access="public" returntype="struct" output="no" hint="Returns all events">
		
		<cfreturn variables.event />
	</cffunction>


	<cffunction name="exists" access="public" returntype="boolean" output="no" hint="Exists a event with the given name">
		<cfargument name="eventName" type="string" required="true" hint="Event name">
		
		<cfif StructKeyExists(variables.event,arguments.eventName)>
			<cfreturn true />
		<cfelse>
			<cfreturn false />
		</cfif>
	</cffunction>
	
	
	<cffunction name="hasListener" access="public" returntype="boolean" output="no" hint="Has the event with the given name listeners">
		<cfargument name="eventName" type="string" required="true" hint="Event name">
		
		<cfif eventExists(arguments.eventName)>
			<cfif ArrayLen(variables.event[arguments.eventName].object.listener) gt 0>
				<cfreturn true />
			<cfelse>
				<cfreturn false />
			</cfif>
		<cfelse>
			<cfreturn false />
		</cfif>
	</cffunction>

	
	<cffunction name="getSetting" access="public" returntype="struct" output="no" hint="Returns the CAEvents configuration">
			
		<cfreturn variables.instance.setting />
	</cffunction>
	
</cfcomponent>