<cfcomponent output="no" name="Event" hint="Event component">
	
	<cffunction name="init" access="public" returntype="struct" output="no" hint="Initializes the object">
		<cfargument name="eventName" type="string" required="true" hint="Event name">
		<cfargument name="eventManager" type="Manager" required="true" hint="Event-Manager instance">
		
		<cfset this.name 				= arguments.eventName>
		<cfset this.listener 			= ArrayNew(1)>
		
		<cfset variables.thread		 	= CreateObject("component","thread").init()>
		<cfset variables.eventManager 	= arguments.eventManager>
		
		<cfreturn this />
    </cffunction>
	
	
	<cffunction name="dispatch" access="public" returntype="any" output="no" hint="Dispatches the current event">
		<cfargument name="eventData" type="any" required="false" default="" hint="Event data">
		
		<cfset local.return 	= arguments.eventData>
		<cfset local.allReturn	= ArrayNew(1)>
		
		<cfloop array="#this.listener#" index="local.item">
			<cfif local.item.async and variables.EventManager.getSetting().allowAsync>
				<cfset variables.Thread.asyncInvoke(local.item.object,local.item.method,this,local.return,arguments.eventData)>
			<cfelse>
				<cfset local.returnHolder = Evaluate("local.item.object.#local.item.method#(argumentCollection=local.return,event=this,rawEventData=arguments.eventData)")>
				<cfif IsDefined("local.returnHolder")>
					<cfset local.logText = local.returnHolder>
				<cfelse>
					<cfset local.logText = "no-return">
				</cfif>
				<cfif variables.eventManager.getSetting().debugMode>
					<cfset ArrayAppend(local.allReturn,{call={object=local.item.object,method=local.item.method},arguments=local.return,returnData=local.logText,rawEventData=arguments.eventData})>
				</cfif>
				<cfif IsDefined("local.returnHolder")> 
					<cfset local.return = local.returnHolder>
				</cfif>
			</cfif>
		</cfloop>
		
		<cfset local.returnData = {rawData=arguments.eventData,allReturn=local.allReturn}>
		<cfif IsDefined("local.return")>
			<cfset local.returnData.return = local.return>
		</cfif>
		
		<cfreturn local.returnData />
	</cffunction>
	
	
	<cffunction name="addListener" access="public" returntype="boolean" output="no" hint="Adds a listener to the current event">
		<cfargument name="object"  type="component" required="true" hint="Object in which a function should be called">
		<cfargument name="method" type="string" required="true" hint="Function name which should be called">
		<cfargument name="key" type="string" required="false" default="" hint="Key, used to remove a listener">
		<cfargument name="async" type="boolean" required="false" default="false" hint="Execute event asynchron (no callback)">
		
		<cfset local.listener = {object=arguments.object,method=arguments.method,key=arguments.key,async=arguments.async}>
		<cfset ArrayAppend(this.listener,local.listener)>
		
		<cfreturn true />
	</cffunction>
	
	
	<cffunction name="removeListener" access="public" returntype="boolean" output="no" hint="Removes a listener from the current event">
		<cfargument name="key" type="string" required="true" hint="Listener key">
		
		<cfset local.listener = Duplicate(this.listener)>
		
		<cfloop array="#local.listener#" index="local.item">
			<cfif local.item.key eq arguments.key>
				<cfset ArrayDelete(this.listener,local.item)>
			</cfif>
		</cfloop>
		
		<cfreturn true />
	</cffunction>
	
	
	<cffunction name="getThread" access="public" returntype="array" output="no" hint="Returns all active threads">
			
		<cfreturn variables.thread.getThread() />
	</cffunction>	
	
</cfcomponent>